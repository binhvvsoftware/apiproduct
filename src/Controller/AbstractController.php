<?php

namespace App\Controller;

/**
 * Class AbstractController
 *
 * @package App\Controller
 */
abstract class AbstractController extends AppController
{
    /**
     * Initialization hook method.
     *
     * @throws \Exception
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Auth', [
            ['className' => 'Session', 'key' => 'BinhVV'],
            'authenticate' => [
                'Form' => [
                    'userModel' => 'User',
                    'fields' => ['username' => 'username', 'password' => 'password'],
                ]
            ],

        ]);
    }
}
