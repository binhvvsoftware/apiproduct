<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Repository\Impl\OrdersRepo as OrdersRepo;
use App\Repository\Impl\UserRepo as UserRepo;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\UserTable $User
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    var $UserRepository;
    var $OrdersRepository;

    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
        $this->UserRepository = new UserRepo(TableRegistry::getTableLocator()->get('User'));
        $this->OrdersRepository = new OrdersRepo($this->Orders);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $aryResponse = [
            'status_code' => 200,
            'message' => [],
            'Data' => [],
            '_serialize' => ['status_code', 'message', 'Data']
        ];
        if ($this->request->is('get')) {
            list($statusToken, $message) = $this->UserRepository->checkTokenUserAuthentication($this->request->getQuery());
            if ($statusToken) {
                $orders = $this->OrdersRepository->getOrders();
                $aryResponse['message'] = 'Lấy thông tin hóa đơn thành công';
                $aryResponse['Data'] = $orders;
            } else {
                $aryResponse['status_code'] = 401;
                $aryResponse['message'] = $message;
            }
        } else {
            $aryResponse['status_code'] = 401;
            $aryResponse['message'] = 'API này chỉ chấp nhận phương thức GET';
        }
        $this->set($aryResponse);
    }
}
