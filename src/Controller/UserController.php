<?php

namespace App\Controller;

use App\Repository\Impl\UserRepo as UserRepo;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * User Controller
 *
 * @property \App\Model\Table\UserTable $User
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserController extends AbstractController
{

    var $UserRepository;

    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
        $this->UserRepository = new UserRepo($this->User);
        $this->Auth->allow();
    }

    public function login()
    {
        $aryResponse = [
            'status_code' => 200,
            'message' => [],
            'Data' => [],
            '_serialize' => ['status_code', 'message', 'Data']
        ];

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if (!$user) {
                $aryResponse['status_code'] = 401;
                $aryResponse['message'] = 'Đăng nhập không thành công! Vui lòng kiểm tra lại Username và Password!';
            } else {
                $aryResponse['message'] = 'Đăng nhập thành công.';
                $aryResponse['Data'] = [
                    'token' => JWT::encode([
                        'sub' => $user['id'],
                        'exp' => time() + 3600, // 1 hour
                        'role' => $user['username'],
                    ],
                    Security::salt())
                ];

                $this->UserRepository->updateTokenUser($user['id'], ['token' => $aryResponse['Data']['token']]);
            }
        } else {
            $aryResponse['status_code'] = 401;
            $aryResponse['message'] = 'API này chỉ chấp nhận phương thức POST';
        }
        $this->set($aryResponse);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $aryResponse = [
            'status_code' => 200,
            'message' => [],
            'Data' => [],
            '_serialize' => ['status_code', 'message', 'Data']
        ];
        if ($this->request->is('get')) {
            list($statusToken, $message) = $this->UserRepository->checkTokenUserAuthentication($this->request->getQuery());
            if ($statusToken) {
                $users = $this->UserRepository->GetAll();
                $aryResponse['message'] = 'Lấy thông tin người dùng thành công';
                $aryResponse['Data'] = $users;
            } else {
                $aryResponse['status_code'] = 401;
                $aryResponse['message'] = $message;
            }
        } else {
            $aryResponse['status_code'] = 401;
            $aryResponse['message'] = 'API này chỉ chấp nhận phương thức GET';
        }
        $this->set($aryResponse);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $aryResponse = [
            'status_code' => 200,
            'message' => [],
            'Data' => [],
            '_serialize' => ['status_code', 'message', 'Data']
        ];
        if ($this->request->is('post')) {
            list($user, $statusAddUser) = $this->UserRepository->addUser($this->request->getData());
            if ($statusAddUser) {
                $aryResponse['message'] = 'Đã tạo thông tin người dùng thành công';
                $aryResponse['Data'] = $user;
                $this->set($aryResponse);
            } else {
                if (!empty($user->getErrors())) {
                    $message = [];
                    foreach ($user->getErrors() as $filed => $aryError) {
                        foreach ($aryError as $iError) {
                            array_push($message, $iError);
                        }
                    }
                    $aryResponse['status_code'] = 401;
                    $aryResponse['message'] = $message;
                }
            }
        } else {
            $aryResponse['status_code'] = 401;
            $aryResponse['message'] = 'API này chỉ chấp nhận phương thức POST';
        }
        $this->set($aryResponse);
    }
}
