<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * User Model
 *
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\TopicsTable|\Cake\ORM\Association\HasMany $Topics
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UserTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->setProvider('custom', 'App\Validation\CustomValidation');
        $validator
            ->scalar('username')
            ->maxLength('username', 10, "Trường username phải tối đa 10 ký tự")
            ->minLength('username', 6, "Trường username phải tối thiểu 6 ký tự")
            ->requirePresence('username', 'create', "Trường username không được để trống")
            ->notEmpty('username')
            ->add('username', [
                'isContainCaps' => [
                    'rule'     => 'isContainCaps',
                    'provider' => 'custom',
                    'message'  => 'Trường username phải chứa ít nhất một ký tự in hoa',
                ],
            ]);
        $validator
            ->scalar('password')
            ->minLength('password', 6, "Trường password phải tối thiểu 6 ký tự")
            ->requirePresence('password', 'create', "Trường password không được để trống")
            ->notEmpty('password');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username'], 'Trường username này đã tồn tại trên hệ thống!'));

        return $rules;
    }
}
