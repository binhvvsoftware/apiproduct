<?php

namespace App\Repository;
/**
 * Created by JetBrains PhpStorm.
 * User: BinhVV
 * Date: 08/28/20
 * Time: 8:42 PM
 * To change this template use File | Settings | File Templates.
 */

interface IOrdersRepo {
    public function getOrders();
}
