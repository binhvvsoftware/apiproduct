<?php

namespace App\Repository;

interface IRepo {
    public function GetById($id);
    public function GetAll();
}
