<?php

namespace App\Repository\Impl;

use App\Repository\IRepo as IRepo;

abstract class BaseRepo implements IRepo
{

    protected $Model;

    public function __construct($model)
    {
        $this->Model = $model;
    }

    public function GetById($id)
    {
        return $this->Model->find('all')->where(['id' => $id])->first();
    }

    public function GetAll()
    {
        return $this->Model->find('all');
    }

}
