<?php

namespace App\Repository\Impl;

use App\Repository\IOrdersRepo as IOrdersRepo;

class OrdersRepo extends BaseRepo implements IOrdersRepo {

    public function getOrders() {
        return $this->Model->find('all')->contain(['Products']);
    }
}
