<?php

namespace App\Repository\Impl;

use App\Repository\IUserRepo as IUserRepo;

class UserRepo extends BaseRepo implements IUserRepo {

    public function addUser($data) {
        $userEntity = $this->Model->newEntity();
        $user = $this->Model->patchEntity($userEntity, $data);

        return [$user, $this->Model->save($user)];
    }

    public function updateTokenUser($userId, $data) {
        $userEntity = $this->GetById($userId);
        $user = $this->Model->patchEntity($userEntity, $data);

        return  $this->Model->save($user);
    }

    public function checkTokenUserAuthentication($dataToken) {
        if (empty($dataToken['token'])) {
            return [false, 'Bạn phải truyền token để thực hiện hành động này'];
        }

        $user = $this->Model->find('all')->where(['token' => $dataToken['token']])->first();

        if (empty($user)) {
            return [false, 'Token cung cấp không đúng, vui lòng đăng nhập lại để nhận Token!'];
        }

        return [true, ''];
    }
}
