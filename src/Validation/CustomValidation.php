<?php

namespace App\Validation;

use Cake\Validation\Validation;

class CustomValidation extends Validation
{
    /**
     * Validate is contain character Cap
     *
     * @param string $character
     *
     * @return bool
     */
    public function isContainCaps($character)
    {
        preg_match_all("/[A-Z]/", $character, $caps_match);
        $capsCount = !empty($caps_match) ? count($caps_match[0]) : 0;
        return $capsCount > 0;
    }
}
